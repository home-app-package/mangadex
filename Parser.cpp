//
// Created by ragbot on 10/3/23.
//

#include "MangaDex/Parser.h"

Parser::Parser() = default;
Parser::~Parser() = default;

Response Parser::parse_manga(std::string buffer) {
    Response response;
    json jsonData;
    try {
        jsonData = json::parse(buffer);

        response.result = setIfNull(jsonData, "result");
        response.response = setIfNull(jsonData, "response");
        response.limit = setIfIntNull(jsonData, "limit");
        response.offset = setIfIntNull(jsonData, "offset");
        response.total = setIfIntNull(jsonData, "total");

        for (const auto& item : jsonData["data"]) {
            Manga dataItem;
            dataItem.id = setIfNull(item, "id");
            dataItem.type = setIfNull(item, "type");

            dataItem.attributes.title.en = setIfNull(item["attributes"]["title"], "en");

            if (item["attributes"].contains("altTitles")) {
                dataItem.attributes.altTitles.clear();
                for (const auto& altTitle : item["attributes"]["altTitles"]) {
                    AltTitle altTitleItem;
                    altTitleItem.en = setIfNull(altTitle, "en");
                    altTitleItem.vi = setIfNull(altTitle, "vi");
                    altTitleItem.ja_ro = setIfNull(altTitle, "ja-ro");
                    altTitleItem.ru = setIfNull(altTitle, "ru");
                    altTitleItem.th = setIfNull(altTitle, "th");
                    altTitleItem.ja = setIfNull(altTitle, "ja");
                    altTitleItem.ko = setIfNull(altTitle, "ko");
                    altTitleItem.zh_hk = setIfNull(altTitle, "zh-hk");
                    dataItem.attributes.altTitles.push_back(altTitleItem);
                }
            }

            if (item.contains("relationships")) {
                dataItem.relationships.clear();
                for (const auto& rel : item["relationships"]) {
                    Relationship relItem;
                    relItem.id = setIfNull(rel, "id");
                    relItem.type = setIfNull(rel, "type");
                    dataItem.relationships.push_back(relItem);
                }
            }

            dataItem.attributes.description.en = setIfNull(item["attributes"]["description"], "en");
            dataItem.attributes.description.ru = setIfNull(item["attributes"]["description"], "ru");

            dataItem.attributes.isLocked = setIfBoolNull(item["attributes"], "isLocked");

            dataItem.attributes.links.al = setIfNull(item["attributes"]["links"], "al");
            dataItem.attributes.links.ap = setIfNull(item["attributes"]["links"], "ap");
            dataItem.attributes.links.bw = setIfNull(item["attributes"]["links"], "bw");
            dataItem.attributes.links.kt = setIfNull(item["attributes"]["links"], "kt");
            dataItem.attributes.links.mu = setIfNull(item["attributes"]["links"], "mu");
            dataItem.attributes.links.amz = setIfNull(item["attributes"]["links"], "amz");
            dataItem.attributes.links.cdj = setIfNull(item["attributes"]["links"], "cdj");
            dataItem.attributes.links.ebj = setIfNull(item["attributes"]["links"], "ebj");
            dataItem.attributes.links.mal = setIfNull(item["attributes"]["links"], "mal");
            dataItem.attributes.links.raw = setIfNull(item["attributes"]["links"], "raw");
            dataItem.attributes.links.engtl = setIfNull(item["attributes"]["links"], "engtl");

            // Handle other fields within the Data struct
            dataItem.attributes.originalLanguage = setIfNull(item["attributes"], "originalLanguage");
            dataItem.attributes.lastVolume = setIfNull(item["attributes"], "lastVolume");
            dataItem.attributes.lastChapter = setIfNull(item["attributes"], "lastChapter");
            dataItem.attributes.publicationDemographic = setIfNull(item["attributes"], "publicationDemographic");
            dataItem.attributes.status = setIfNull(item["attributes"], "status");
            dataItem.attributes.year = setIfIntNull(item["attributes"], "year");
            dataItem.attributes.contentRating = setIfNull(item["attributes"], "contentRating");

            // Handle "availableTranslatedLanguages" field
            if (item["attributes"].contains("availableTranslatedLanguages")) {
                dataItem.availableTranslatedLanguages.clear();
                for (const auto& lang : item["attributes"]["availableTranslatedLanguages"]) {
                    dataItem.availableTranslatedLanguages.push_back(setIfNull(lang, ""));
                }
            }

            // Handle "latestUploadedChapter" field
            dataItem.latestUploadedChapter = setIfNull(item["attributes"], "latestUploadedChapter");

            response.data.push_back(dataItem);
        }
    } catch (const json::exception& e) {
        std::cerr << "JSON parsing error: " << e.what() << std::endl;
        response = {
                .response = "error",
                .total = 0
        };
    }
    return response;
}
ResponseC Parser::parse_chapter_list(std::string buffer) {
    // Parse the JSON string into a JSON object
    json jsonData = json::parse(buffer);

    // Create a ResponseC object and parse data into it
    ResponseC responseC;
    responseC.result = setIfNull(jsonData, "result");
    responseC.response = setIfNull(jsonData, "response");
    responseC.limit = setIfIntNull(jsonData, "limit");
    responseC.offset = setIfIntNull(jsonData, "offset");
    responseC.total = setIfIntNull(jsonData, "total");

    // Iterate through Chapter objects and parse values into structs
    for (const auto& chapter : jsonData["data"]) {
        Chapter chapterData;

        // Parse Chapter values
        chapterData.id = setIfNull(chapter, "id");
        chapterData.type = setIfNull(chapter, "type");

        // Parse ChapterAttributes values using helper functions
        chapterData.attributes.volume = setIfNull(chapter["attributes"], "volume");
        chapterData.attributes.chapter = setIfNull(chapter["attributes"], "get_chapter");
        chapterData.attributes.title = setIfNull(chapter["attributes"], "title");
        chapterData.attributes.translatedLanguage = setIfNull(chapter["attributes"], "translatedLanguage");
        chapterData.attributes.externalUrl = setIfNull(chapter["attributes"], "externalUrl");
        chapterData.attributes.publishAt = setIfNull(chapter["attributes"], "publishAt");
        chapterData.attributes.readableAt = setIfNull(chapter["attributes"], "readableAt");
        chapterData.attributes.createdAt = setIfNull(chapter["attributes"], "createdAt");
        chapterData.attributes.updatedAt = setIfNull(chapter["attributes"], "updatedAt");
        chapterData.attributes.pages = setIfIntNull(chapter["attributes"], "pages");
        chapterData.attributes.version = setIfIntNull(chapter["attributes"], "version");

        // Iterate through relationships and parse values
        for (const auto& relationship : chapter["relationships"]) {
            Relationship rel;
            rel.id = setIfNull(relationship, "id");
            rel.type = setIfNull(relationship, "type");
            chapterData.relationships.push_back(rel);
        }

        // Add parsed Chapter to the ResponseC data vector
        responseC.data.push_back(chapterData);
    }

    return responseC;
}

ChapterData Parser::parse_chapter(std::string buffer)
{
    auto json_data = json::parse(buffer);
    ChapterData chapterData;
    chapterData.result = setIfNull(json_data, "result");
    chapterData.baseUrl = setIfNull(json_data, "baseUrl");
    chapterData.chapter.hash = setIfNull(json_data["chapter"], "hash");

    chapterData.chapter.data.data = setIfArrayNull(json_data["chapter"], "data");
    chapterData.chapter.data.dataSaver = setIfArrayNull(json_data["chapter"], "dataSaver");

    return chapterData;
}

std::string Parser::parse_cover(std::string buffer)
{
    json jsonData = json::parse(buffer);
    return setIfNull(jsonData["data"]["attributes"], "fileName");
}