//
// Created by ragbot on 9/22/23.
//

#ifndef MANGADEX_H
#define MANGADEX_H

#include <curl/curl.h>
#include <nlohmann/json.hpp>

#include <iostream>
#include <stdexcept>
#include <vector>
#include <format>

#include "Parser.h"

class MangaDex {
public:
    MangaDex();
    ~MangaDex();

    /**
     * Search for manga on MangaDex based on the title.
     * @param title The title of the manga to search for (default is an empty string).
     * @return A vector of Manga objects matching the search query.
     */
    std::vector<Manga> search(std::string title = "");
    /**
     * Get get_chapter_list for a specific manga.
     * @param manga The Manga object for which to retrieve get_chapter_list.
     * @return A ResponseC object containing get_chapter information.
     * @throws std::runtime_error if manga ID is empty.
     */
    ResponseC get_chapter_list(Manga manga);

    /**
     * Retrieve chapter pages for a specific get_chapter.
     * @param chapter The Chapter object for which to retrieve pages.
     * @return A vector of URLs for get_chapter pages.
     */
    std::vector<std::string> get_chapter(Chapter chapter, DataSaver dataSaver = false);
    std::vector<std::vector<uint8_t>> get_chapter_data(Chapter chapter, DataSaver dataSaver = false);
    /**
     * Get cover art url for a specific manga.
     * @param manga The Manga object for which to retrieve cover art.
     * @param size The desired size of the cover art (default is LARGE).
     * @return The URL of the cover art image.
     */
    std::string get_cover(Manga manga, CoverSize size = LARGE);

    /**
     * Get cover art url for a specific manga.
     * @param manga The Manga object for which to retrieve cover art.
     * @param size The desired size of the cover art (default is LARGE).
     * @return The URL of the cover art image.
     */
    std::vector<uint8_t> get_cover_data(Manga manga, CoverSize size = LARGE);

private:
    bool get(std::string url);
    static std::string param_gen(Params const &params, std::string url = "")
    {
        auto rm_func = [](std::string str, const std::string& from, const std::string& to){
            size_t start_pos = 0;
            while((start_pos = str.find(from, start_pos)) != std::string::npos) {
                str.replace(start_pos, from.length(), to);
                start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
            }
            return str;
        };
        std::string data = url;
        if (params.empty()) return data;
        auto param_begin = params.cbegin()
        , param_end = params.cend();

        data += "?" + rm_func(param_begin->first, " ", "%20") + "="
                           + rm_func(param_begin->second, " ", "%20");
        ++param_begin;
        if (param_begin == param_end) return data;
        for (; param_begin != param_end; ++param_begin)
            data += "&" + param_begin->first + "=" +
                    param_begin->second;
        return data;
    }

    static size_t write_get_data(void *buffer, size_t size, size_t nmemb, void *userp)
    {
        ((std::string *) userp)->append((char *) buffer, size * nmemb);
        return size * nmemb;
    }
    static size_t image_write_get_data(void* contents, size_t size, size_t nmemb, void* userp) {
        size_t total_size = size * nmemb;
        uint8_t* data = static_cast<uint8_t*>(contents);
        std::vector<uint8_t>* data_ptr = static_cast<std::vector<uint8_t>*>(userp);
        data_ptr->insert(data_ptr->end(), data, data + total_size);
        return total_size;
    }

    CURL *curl;
    CURLcode response;

    const std::string base_url = "https://api.mangadex.org/manga";
    std::string buffer;
};


#endif //MANGADEX_H
