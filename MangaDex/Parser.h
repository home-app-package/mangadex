//
// Created by ragbot on 10/3/23.
//

#ifndef MANGADEX__PARSER_H
#define MANGADEX__PARSER_H

#include <iostream>
#include <nlohmann/json.hpp>

#include "types.h"

using json = nlohmann::json;

class Parser {
public:
    Parser();
    ~Parser();

    static Response parse_manga(std::string buffer);
    static ResponseC parse_chapter_list(std::string buffer);
    static ChapterData parse_chapter(std::string buffer);
    static std::string parse_cover(std::string buffer);
private:

    static std::string setIfNull(const json& obj, const std::string& key) {
        if (obj.contains(key) && !obj[key].is_null()) {
            return obj[key].get<std::string>();
        }
        return "";
    }

    static int setIfIntNull(const json& obj, const std::string& key) {
        if (obj.contains(key) && !obj[key].is_null()) {
            return obj[key].get<int>();
        }
        return 0;
    }

    static bool setIfBoolNull(const json&obj, const std::string& key) {
        if (obj.contains(key) && !obj[key].is_null()) {
            return obj[key].get<bool>();
        }
        return false;
    }

    static std::vector<std::string> setIfArrayNull(const json& obj, const std::string& key) {
        std::vector<std::string> result;
        if (obj.contains(key) && obj[key].is_array()) {
            for (const auto& element : obj[key]) {
                if (element.is_string()) {
                    result.push_back(element.get<std::string>());
                }
            }
        }
        return result;
    }
};

#endif //MANGADEX__PARSER_H
