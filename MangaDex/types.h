//
// Created by ragbot on 9/27/23.
//

#ifndef MANGADEX_C_TYPES_H
#define MANGADEX_C_TYPES_H

#include <string>
#include <map>

typedef std::map<std::string, std::string> Params;

/// Manga ///
struct Title {
    std::string en;
};

struct AltTitle {
    std::string en;
    std::string vi;
    std::string ja_ro;
    std::string ru;
    std::string th;
    std::string ja;
    std::string ko;
    std::string zh_hk;
};

struct Description {
    std::string en;
    std::string ru;
};

struct Links {
    std::string al;
    std::string ap;
    std::string bw;
    std::string kt;
    std::string mu;
    std::string amz;
    std::string cdj;
    std::string ebj;
    std::string mal;
    std::string raw;
    std::string engtl;
};

struct Attributes {
    Title title;
    std::vector<AltTitle> altTitles;
    Description description;
    bool isLocked;
    Links links;
    std::string originalLanguage;
    std::string lastVolume;
    std::string lastChapter;
    std::string publicationDemographic;
    std::string status;
    int year;
    std::string contentRating;
};

struct Relationship {
    std::string id;
    std::string type;
};

struct Manga {
    std::string id;
    std::string type;
    Attributes attributes;
    std::vector<Relationship> relationships;
    std::vector<std::string> availableTranslatedLanguages;
    std::string latestUploadedChapter;
};

struct Response {
    std::string result;
    std::string response;
    std::vector<Manga> data;
    int limit;
    int offset;
    int total;
};

/// Chapter ///

    struct ChapterAttributes {
        std::string volume;
        std::string chapter;
        std::string title;
        std::string translatedLanguage;
        std::string externalUrl;
        std::string publishAt;
        std::string readableAt;
        std::string createdAt;
        std::string updatedAt;
        int pages;
        int version;
    };

    struct Chapter {
        std::string id;
        std::string type;
        ChapterAttributes attributes;
        std::vector<Relationship> relationships;
    };

    struct ResponseC {
        std::string result;
        std::string response;
        std::vector<Chapter> data;
        int limit;
        int offset;
        int total;
    };

/// CHAPTER DATA ///
    struct ChapterData {
        std::string result;
        std::string baseUrl;
        struct Chapter {
            std::string hash;
            struct Data {
                std::vector<std::string> data;
                std::vector<std::string> dataSaver;
            } data;
        } chapter;
    };
    typedef bool DataSaver;
/// COVER ///

    typedef enum {
        LARGE,
        SMALL,
        SMALLEST
    } CoverSize;

#endif //MANGADEX_C_TYPES_H
