//
// Created by ragbot on 9/22/23.
//

#include "MangaDex/MangaDex.h"
#include "MangaDex/types.h"
#include <iterator>

MangaDex::MangaDex() {
    curl = curl_easy_init();
    curl_easy_setopt(curl, CURLOPT_URL, base_url.c_str());
    curl_easy_setopt(curl, CURLOPT_USERAGENT, "MangaDex_LIB / 1.0 request_made_from_mangadex_c++_lib");
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, this->write_get_data);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);

    response = CURLE_OK;
}

MangaDex::~MangaDex() {
    curl_easy_cleanup(curl);
}

std::vector<Manga> MangaDex::search(std::string title) {
    buffer.clear();
    get (title.empty() ?
        base_url :
        param_gen({{"title", title}}, base_url)
    );
    if (response != CURLE_OK) {
        std::cerr << "Response != CURLE_OK" << std::endl;
        return {};
    }
    return Parser::parse_manga(buffer).data;
}

ResponseC MangaDex::get_chapter_list(Manga manga) {
    std::string id = manga.id;

    if (id.empty()) std::runtime_error("Manga ID is null in get_chapter_list function");

    id.insert(0, "/");
    get(base_url + id + "/feed?limit=250");

    return Parser::parse_chapter_list(buffer);
}


std::vector<std::string> MangaDex::get_chapter(Chapter chapter, DataSaver dataSaver)
{
    std::vector<std::string> image_urls;
    get("https://api.mangadex.org/at-home/server/" + chapter.id);

    ChapterData cd = Parser::parse_chapter(buffer);
    auto image_file_names = (dataSaver) ?
                            cd.chapter.data.dataSaver :
                            cd.chapter.data.data;

    for (const auto file_name: image_file_names )
    {
        image_urls.push_back(
                cd.baseUrl +
                ((dataSaver) ? "/data-saver/" : "/data/") +
                cd.chapter.hash + "/" +
                file_name
                );
    }

    return image_urls;
}

std::vector<std::vector<uint8_t>> MangaDex::get_chapter_data(Chapter chapter, DataSaver dataSaver)
{
    std::vector<std::vector<uint8_t>> image_data;
    std::vector<std::string> urls = get_chapter(chapter, dataSaver);

    CURL* curl_ = curl_easy_init();
    curl_easy_setopt(curl_, CURLOPT_WRITEFUNCTION, image_write_get_data);

    for (int i = 0; i < urls.size(); i++) {
        std::vector<uint8_t> image;  // Create a new vector for each image
        curl_easy_setopt(curl_, CURLOPT_URL, urls.at(i).c_str());
        curl_easy_setopt(curl_, CURLOPT_WRITEDATA, &image);  // Pass the address of the vector

        CURLcode response_ = curl_easy_perform(curl_);

        if (response_ != CURLE_OK)
            std::cerr
                    << "Error on get_chapter_data(): "
                    << curl_easy_strerror(response_)
                    << std::endl;
        else
            image_data.push_back(image);  // Add the downloaded image to the result vector
    }

    curl_easy_cleanup(curl_);
    return image_data;
}

std::string MangaDex::get_cover(Manga manga, CoverSize size)
{
    std::string manga_cover_id = "";
    for (auto i = manga.relationships.begin(); i != manga.relationships.end(); ++i)
        if (i->type == "cover_art")
            manga_cover_id = i->id;
    if (manga_cover_id.empty())
    {
        std::cerr << "Cover Art not found" << std::endl;
        return manga_cover_id;
    }
    get(std::format(
            "https://api.mangadex.org/cover/{}"
            , manga_cover_id));

    std::string file_name = Parser::parse_cover(buffer);

    std::string size_strings[] = {"", ".512", ".256"};

    file_name += size_strings[size];
    if (size > CoverSize::LARGE) file_name +=  ".jpg";
    return std::format("https://uploads.mangadex.org/covers/{}/{}"
                        , manga.id
                        , file_name);
}


std::vector<uint8_t> MangaDex::get_cover_data(Manga manga, CoverSize size) {
    std::string cover_url = get_cover(manga, size);
    std::vector<uint8_t> image_data = {};

    CURL* curl_ = curl_easy_init();

    curl_easy_setopt(curl_, CURLOPT_URL, cover_url.c_str());
    curl_easy_setopt(curl_, CURLOPT_WRITEDATA, &image_data);
    curl_easy_setopt(curl_, CURLOPT_WRITEFUNCTION, image_write_get_data);

    CURLcode response_ = curl_easy_perform(curl_);

    if (response_ != CURLE_OK)
        std::cerr
            << "Error on get_cover_data(): "
            << curl_easy_strerror(response_)
            << std::endl;

    curl_easy_cleanup(curl_);
    return image_data;
}

bool MangaDex::get(std::string url) {
    buffer.clear();

    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    response = curl_easy_perform(curl);

    return response == CURLE_OK;
}
